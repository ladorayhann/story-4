from django.shortcuts import render

# Create your views here.

def biodata(request):
	return render(request, 'biodata.html')

def resume(request):
	return render(request, 'resume.html')

def about(request):
	return render(request, 'about.html')

def galeri(request):
	return render(request, 'galeri.html')

def contact(request):
	return render(request, 'contact.html')