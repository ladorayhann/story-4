from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.biodata, name='biodata'),
    path('resume/', views.resume, name='resume'),
    path('about/', views.about, name='about'),
    path('galeri/', views.galeri, name='galeri'),
    path('contact/', views.contact, name='contact'),

    
]